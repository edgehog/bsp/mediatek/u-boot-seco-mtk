
#ifndef _SYSDATA_H_
#define _SYSDATA_H_


/* Note: use USE_HOSTCC to determinate if we are compiling
 * for u-boot or for external tool (in this case these is present) 
 */
#include <linux/kconfig.h>
#ifndef USE_HOSTCC
#include <common.h>
#else
#include <sys/types.h>
#include <stdint.h>
#include <ctype.h>
#endif

#include <system_data.h>

#endif     /*  _SYSDATA_H_  */

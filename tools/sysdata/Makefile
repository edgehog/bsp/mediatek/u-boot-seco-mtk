#
# (C) Copyright 2002-2006
# Wolfgang Denk, DENX Software Engineering, wd@denx.de.
#
# SPDX-License-Identifier:	GPL-2.0+
#

# fw_sysdata is supposed to run on the target system, which means it should be
# built with cross tools. Although it may look weird, we only replace "HOSTCC"
# with "CC" here for the maximum code reuse of scripts/Makefile.host.
override HOSTCC = $(CC)

# Compile for a hosted environment on the target
HOST_EXTRACFLAGS  = $(patsubst -I%,-idirafter%, $(filter -I%, $(UBOOTINCLUDE))) \
		-idirafter $(srctree)/tools/sysdata \
		-DUSE_HOSTCC \
		-DTEXT_BASE=$(TEXT_BASE) -DEENV_OFFSET=$(ENV_OFFSET)

HOSTLDFLAGS = -static 

ifeq ($(MTD_VERSION),old)
HOST_EXTRACFLAGS += -DMTD_OLD
endif

always := fw_sysdata
hostprogs-y := fw_sysdata

lib-y += \
	crc32.o ctype.o linux_string.o aes.o \
	fw_sysdata_helper.o fw_sysdata_io.o

fw_sysdata-objs := $(lib-y) fw_sysdata.o

quiet_cmd_crosstools_strip = STRIP   $^
      cmd_crosstools_strip = $(STRIP) $^; touch $@

$(obj)/.strip: $(obj)/fw_sysdata 
	$(call cmd,crosstools_strip)

always += .strip
